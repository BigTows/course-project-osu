function Notify(NotifyProperty) {
    var StyleText = "";
    var docEl = document.documentElement;
    var NotifyElement = document.createElement("div");
    NotifyElement.setAttribute("class", "Notify");
    if (NotifyProperty.Style != undefined) {
        NotifyElement.setAttribute("class", (NotifyElement.getAttribute("class") + " " + NotifyProperty.Style));
    }
    var NotifyTextElement = document.createElement("a");
    NotifyTextElement.innerHTML = NotifyProperty.Text;
    document.body.appendChild(NotifyElement);
    NotifyElement.appendChild(NotifyTextElement);


    switch (NotifyProperty.Position.toLowerCase()) {
        case "center":
            if ((NotifyProperty.width == undefined) || (NotifyProperty.height == undefined)) {
                StyleText += "margin-top: " + ((docEl.clientHeight - NotifyElement.clientHeight - 50) / 2) + "px; margin-left:" + ((docEl.clientWidth - NotifyElement.clientWidth - 50) / 2) + "px;";
            } else {
                StyleText += "margin-top: " + ((docEl.clientHeight - NotifyProperty.height - NotifyProperty.height / 2) / 2) + "px; margin-left:" + ((docEl.clientWidth - NotifyProperty.width - NotifyProperty.width / 2) / 2) + "px;";
            }
            break;
        case "lefttop":
            StyleText += "margin: 0 0 0 0;";
            break;
        case "righttop":
            StyleText += "margin: 0 0 0 " + (docEl.clientWidth - NotifyElement.clientWidth - 15) + "px;";
            break;
    }
    if (!(NotifyProperty.width == undefined) || (NotifyProperty.height == undefined)) {
        StyleText += "width: " + NotifyProperty.width + "px; height: " + NotifyProperty.height + "px;";
    }

    if (NotifyProperty.input != undefined) {
        var Form = document.createElement("form");
        var Input = document.createElement("input");
        Form.setAttribute("action", NotifyProperty.input.action);
        Form.setAttribute("method", "POST");
        Input.setAttribute("required", "");
        Input.setAttribute("name", NotifyProperty.input.name);
        Input.setAttribute("type", NotifyProperty.input.type);
        NotifyTextElement.appendChild(Form);
        Form.appendChild(Input);
    }
    NotifyElement.setAttribute("style", StyleText);
    if (NotifyProperty.Close != undefined) {
        setTimeout(function () {
            document.body.removeChild(NotifyElement);
        }, NotifyProperty.Close * 1000);
    }
}


/*window.onclick = function () {
    var ClikedElement = document.elementFromPoint(event.clientX, event.clientY);
    if (NotifyProp.Focus) {
        if (ClikedElement.getAttribute("class") == null) {
            return false;
        }
        if (ClikedElement.getAttribute("class").substring(0, 6) != "Notify") {
            return false;
        }
    }
}
*/
