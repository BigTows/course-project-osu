/**
* @member {HTML_DOM} Select Hook Selector on html
*/
var Select = document.getElementById("Selector");
Select.addEventListener("change", Update);
function Update() {
	getRecords(Select.value);
}
/**
 * Return table with Records on Map
 * @param {String} Map Name Map
 */
function getRecords(Map) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', 'Utilis/Records.php');
	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;
		var Block = document.getElementById("Records");
		Block.innerHTML = this.responseText;
	}
	var Form = new FormData();
	Form.append("Map",Map);
	xhr.send(Form);
	xhr = null;
}