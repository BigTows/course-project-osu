"use strict";
/**
* @author BigTows
* @version 1.4
*/
window.onkeypress = NodeSpawn;
window.onmousemove = Move;
window.onmousedown = Moves;
/**
* @member {HTML_DOM} Music Music Element
*/
var Music = document.getElementById("Music");
Music.addEventListener("change", LoadMusic);
/**
* @member {HTML_DOM} NameLevel Name Level 
*/
var NameLevel = document.getElementById("NameLevel");
NameLevel.addEventListener("focus", function() {
	EditOn = false;
});
NameLevel.addEventListener("focusout", function() {
	EditOn = true;
});
/**
* @member {HTML_DOM} Player Music File
*/
var Player = document.getElementById("Player");
/**
* @member {Array} ArrayNodes save all Nodes Spawn User
*/
var ArrayNodes = [];
/**
* @member {integer} NumElem Counter Nodes
*/
var NumElem = 0;
var EditOn = true;
var NowTime;
var LateTime;
var Delay = 0;
var ReturnText;
var MusicFile = null;
var MoveObj = {
	Pick: false,
	Num: 0
}
var Pick = false;
/**
* @member {HTML_DOM} Scene where spawn Elements and Action
*/
var Scene = document.getElementById("Zone");
/**
 * @description This Object save Cords Mouse user
* @borrows 0 as X
* @borrows 0 as Y
*/
var Mouse = {
	X: 0,
	Y: 0
}
/**
* @classdesc Action Class save Type Action,Delay and Node
*/
class Actions {
	/**
	 * @constructor
	 * @param {String}  Type  Type Element
	 * @param {integer} Delay Delay for Timer on Osu.js
	 */
	constructor(Type, Delay) {
		this._Type = Type;
		this._Delay = Delay;
		this._Node = document.createElement("div");
	}
}
/**
* @classdesc Class Element 
* @augments Actions
*/
class Element extends Actions {
	/**
	 * Spawn and save Node on Scree n
	 * @param {String} Name Type Element
	 */
	Create(Name) {
		this._Node.className = Name;
		this._Node.setAttribute("onclick", "MoveBall(" + NumElem + ")");
		Scene.appendChild(this._Node);
		this._Pick = false;
	}
    /**
     * @description set Cord Node on x
    * @param {integer} Cord x on Screen
    */
	set CordX(X) {
		this._CordX = X;
		this._Node.style.marginLeft = X;
	}
    /**
     * @description set Cord Node on y
    * @param {integer} Cord y on Screen
    */
	set CordY(Y) {
		this._CordY = Y;
		this._Node.style.marginTop = Y;
	}
	/**
	 * @description When User Click on Node he can move this node, this function Switcher
	 */
	Pick() {
		if (this._Pick) {
			this._Pick = false;
			this._Node.style.border = "4px solid #fff";
		} else {
			this._Pick = true;
			this._Node.style.border = "4px dotted #fff";
		}
	}
    /**
    * @description if Node Pick return true else false
    */
	get isPick() {
		return this._Pick;
	}
    /**
    * @description Return Node
    */
	get Node() {
		return this._Node;
	}
}
/**
 * @description Spy User cords mouse
 */
function Move() {
	Mouse.X = event.clientX - 25;
	Mouse.Y = event.clientY - 75;
}
/**
 * @description When user KeyPress Spawning Node
 */
function NodeSpawn() {
	if (!EditOn) {
		return;
	}
	if (NumElem == 0) {
		NowTime = new Date();
		LateTime = NowTime;
	} else {
		LateTime = NowTime;
		NowTime = new Date();
		NowTime.getTime();
	}
	Delay = NowTime - LateTime;
	var key = String.fromCharCode(event.keyCode);
	//alert(String.fromCharCode(event.keyCode));
/*
        32 - Space
        99 - 'C'
     */
	switch (key.toLowerCase()) {
	case " ":
		CreateElement("ball", Delay);
		NumElem++;
		break;
	case "c":
		RemoveAllNode();
		CreateAction("clean", Delay);
		NumElem++;
		break;
	case "d":
		{
			CreateAction("delay", Delay);
			NumElem++;
			break;
		}
	case "s":
		{
			CreateElement("spin", Delay);
			NumElem++;
			break;
		}
	case "f":
		{
			SendResult();
			NumElem++;
			break;
		}
	case "w":
		{
			RemoveAllNode();
			break;
		}
	case "v":
		//CreateAction("")
		break;
	}
/*switch (key) {
        case 32:
            CreateBall();
    }*/
	//alert(key);
}
/**
 * @description Move Node if User pick 
 */
function Moves() {
	if (ArrayNodes[MoveObj.Num] != undefined) {
		if (ArrayNodes[MoveObj.Num].isPick) {
			ArrayNodes[MoveObj.Num].CordX = (Mouse.X);
			ArrayNodes[MoveObj.Num].CordY = (Mouse.Y);
		}
	}
}
/**
 * @description Clean Screen
 */
function RemoveAllNode() {
	for (var i = 0; i < NumElem; i++) {
		if (ArrayNodes[i] != undefined) {
			if (ArrayNodes[i].Node != undefined) {
				ArrayNodes[i].Node.remove();
			}
		}
	}
}
/**
 * Move Ball
 * @param {integer} Num Num in Array Nodes
 */
function MoveBall(Num) {
	if (Num != MoveObj.Num) {
		ArrayNodes[MoveObj.Num].Pick();
	}
	if (ArrayNodes[Num].isPick) {
		ArrayNodes[Num].Pick();
	} else {
		MoveObj.Num = Num;
		ArrayNodes[Num].Pick();
	}
}
/**
 * Create Element
 * @param {String}  Name  Type of Element
 * @param {integer} Delay Delay for Timer
 */
function CreateElement(Name, Delay) {
	ArrayNodes[NumElem] = new Element(Name, Delay);
	if (Name == "spin") {
		Mouse.Y = window.innerHeight / 2 - 349 / 2;
		Mouse.X = window.innerWidth / 2 - 349 / 2;
	}
	ArrayNodes[NumElem].CordX = Mouse.X;
	ArrayNodes[NumElem].CordY = Mouse.Y;
	ArrayNodes[NumElem].Create(Name);
}
/**
 * Create Action
 * @param {String}  Name  Type Action
 * @param {integer} Delay Delay for Timer
 */
function CreateAction(Name, Delay) {
	ArrayNodes[NumElem] = new Actions(Name, Delay);
}
/**
 * Load Music to var Player
 * @param {object} e Event
 */
function LoadMusic(e) {
	var Files = e.target.files;
	if (Files[0]==undefined){
		return;
	}
	var FileName = URL.createObjectURL(e.target.files[0]);
	if (Files.length != 1) {
		alert("Файл должен быть 1");
	} else if (Files[0].type!="audio/mp3") {
		alert("Поддерживается только mp3");
	} else {
		Player.src = FileName;
		Player.load();
		MusicFile = e.target.files[0];
	}
}
/**
 * @description Return Sourse Map to server
 */
function SendResult() {
	
	if (MusicFile == null) {
		alert("Файл не выбран!");
		console.log("File don't pick!");
		return;
	}
	
	if ((NameLevel.value.match(/^[a-zA-Z](.[a-zA-Z0-9_-]*)$/)==null) || (NameLevel.value.length<4)){
		alert("Название имеет не верное значение");
		console.log("Bad name map");
		return;
	}
	
	var Data = new FormData();
	var TestArray = ArrayNodes;
	for (var i = 0; i < NumElem; i++) {
		delete TestArray[i]._Node;
		delete TestArray[i]._Pick;
	}
	var js = (JSON.stringify(TestArray));
	Data.append("LevelJSON", js);
	Data.append("Music", MusicFile);
	//Fix BackDoor ..
	Data.append("UserName", ClientName);
	//Fix BackDoor ..
	Data.append("LevelName", NameLevel.value);
	Data.append("WindowH", document.body.clientHeight);
	Data.append("WindowW", document.body.clientWidth);
	var xhr = new XMLHttpRequest();
	xhr.open('post', 'Utilis/SendMap.php', true);
	//xhr.setRequestHeader("Content-Type", "multipart/form-data");
	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;
		console.log(this.responseText);
		//alert("Ваша карта была сохранена!");
	}
	xhr.send(Data);
	//xhr = null;
}