"use strict";
/**
* @author BigTows
* @version 1.4
*/

/**
* @member {integer} StepNum Counter level steps
*/
var StepNum = 0;
/**
 * @description This Object use for control Spiner
* @borrows false as Enable
* @borrows 0 as NumElement
* @borrows 0 as Sector
*/
var SpinObject = {
	Enable: false,
	NumElem: 0,
	Sector: 0
}
/**
* @member {integer} RoomBall This var use for inner for Ball
*/
var RoomBall = 1;

/**
 * @member {HTMLDOM} ZoneGame Zone Game where appear objects
*/
var ZoneGame = document.getElementById("Zone");

/**
 * @member {HTMLDOM} Audio Audio element, control
*/
var Audio = document.getElementById("Music");
/**
 * @member {Array} NodeElems Struct Level
*/
var NodeElems = [];
/**
 * @classdesc This User Class stores all necessary information about the user
*/
class User {
	/**
	 * @description Create User
	 * @class
	 * @param {String}  Name       User Name
	 * @param {integer} Complexity Complexity game
	 */
	constructor(Name, Complexity) {
		this._NodeScore = document.getElementById("Score");
		this._UserName = Name;
		this._ScreenX = window.innerWidth;
		this._ScreenY = window.innerHeight;
		this._Score = 0;
		this._Step = 0;
		this._Complexity = Complexity;
	}
    /**
    * @description get Screen Width User
    */
	get ScreenW() {
		return this._ScreenX;
	}
    /**
    * @description get Screen Height User
    */
	get ScreenH() {
		return this._ScreenY;
	}
    /**
    * @description get User Name
    */
	get Name() {
		return this._UserName;
	}
    /** 
     * @description set Step need for control user pick objects
    * @param {integer} set Step
    */
	set Step(Num) {
		this._Step = Num;
	}
    /**
    * @description get User step
    */
	get Step() {
		return this._Step;
	}
    /**
    * @description get Score User
    */
	get Score() {
		return this._Score;
	}
    /**
    * @description get Complexity User
    */
	get Complexity() {
		return this._Complexity;
	}
	/**
	 * @description add Point to User Score
	 * @param {integer} Points the right amount to add points
	 */
	addScore(Points) {
		this._Score += Points;
		this._NodeScore.firstChild.nodeValue = "Score: " + this._Score;
	}
	/**
	 * @description add 1 Step
	 */
	addStep() {
		this._Step++;
	}
}
/**
* @classdesc Class Node, use for Spin and Balls
*/
class Node {
	/**
	 * @description Create Object with cords
	 * @param {integer} CordX Cord on X
	 * @param {integer} CordY Cord on Y
	 */
	constructor(CordX, CordY) {
		this._CordX = CordX;
		this._CordY = CordY;
		this._Exist = false;
		this._Node = document.createElement("div");
	}
	/**
	 * @description Hide the Node from Zone Game 
	 */
	Remove() {
		this._Node.remove();
		this._Exist = false;
	}
    /**
    * @description is Show on Zone Game
    */
	get Exist() {
		return this._Exist;
	}
}
/**
* @classdesc Class Ball 
* @augments Node
*/
class Ball extends Node {
	/**
	 * @description Create Ball (Node) 
	 * @param {integer} CordX Cord on X
	 * @param {integer} CordY Cord on Y
	 */
	constructor(CordX, CordY) {
		super(CordX, CordY);
		this._Circle = document.createElement("div");
		this._Num=StepNum;
	}
	/**
	 * @description Show Ball and Create Circle for Ball
	 */
	Create() {
		this._Node.className = "ball";
		this._Node.style.marginTop = this._CordY * (Usr.ScreenH * 100 / Setting.Window.height) / 100;
		this._Node.style.marginLeft = this._CordX * (Usr.ScreenW * 100 / Setting.Window.width) / 100;
		this._Node.setAttribute("onclick", "Click("+this._Num+")");
		this._NodeTest = document.createElement("a");
		this._NodeTest.innerHTML = RoomBall;
		this._Node.appendChild(this._NodeTest);
		//Create Circle
		this._Circle.className = "Circle";
		this._Circle.style.marginTop = (this._CordY * (Usr.ScreenH * 100 / Setting.Window.height) / 100) - 23.5;
		this._Circle.style.marginLeft = (this._CordX * (Usr.ScreenW * 100 / Setting.Window.width) / 100) - 23.5;
		this._Circle.style.animation = "CircleAn " + Usr.Complexity + "s ease";
		this._Exist = true;
		ZoneGame.appendChild(this._Node);
		ZoneGame.appendChild(this._Circle);
		TimerRemove(this._Node, this._Circle,this._Num);
	}
	/**
	 * @description Hide the Node from Zone Game 
	 */
	Remove() {
		this._Node.remove();
		this._Circle.remove();
		this._Exist = false;
	}
    /**
    * @description Number Node
    */
	get NumNode(){
		return this._Num;
	}
    /**
    * @description Calculate and return Score for this Node
    */
	get Score(){
		return Math.round((parseFloat(getComputedStyle(this._Circle).width, 10) * 2));
	}
}
/**
* @classdesc Class Spin
* @augments Node
*/
class Spin extends Node {
	/**
	 * @description Create Spin on Screen and set Middle Cords (x and y)
	 */
	Create() {
		this._Speed = 20;
		this._Deg = 0;
		this._Node.className = "Spin";
		this._Node.style.marginTop = (Usr.ScreenH - 349) / 2;
		this._Node.style.marginLeft = (Usr.ScreenW - 349) / 2;
		this._Exist = true;
		ZoneGame.appendChild(this._Node);
	}
    /**
    * @description Set Speed
    */
	set Speed(NewSpeed) {
		this._Speed = NewSpeed;
	}
    /**
     * @description Return Speed Spin
    */
	get Speed() {
		return this._Speed;
	}
    /**
    * @description Set Deg Spin
    */
	set Deg(NewDeg) {
		this._Deg = NewDeg;
	}
    /**
    * @description Return Deg Spin
    */
	get Deg() {
		return this._Deg;
	}
    /**
    * @description Return Node Spin
    */
	get Spin() {
		return this._Node;
	}
}
/**
 * @description This Starter Function. This recursive function. Execute with Timer
 */
function Step() {
	switch (StepLevel[StepNum]._Type) {
	case 'ball':
		{
			NodeElems[StepNum] = new Ball(StepLevel[StepNum]._CordX, StepLevel[StepNum]._CordY);
			NodeElems[StepNum].Create();
			RoomBall++;
			break;
		}
	case 'clean':
		{
			for (var i = Usr.Step; i < StepNum; i++) {
             try{
                NodeElems[i].Remove();
             }catch(e){
                 console.log("Sorry Error, but we continue");
             }  
            }
            
			Usr.Step = i + 1;
			RoomBall = 1;
			break;
		}
	case 'spin':
		{
			NodeElems[StepNum] = new Spin(StepLevel[StepNum]._CordX, StepLevel[StepNum]._CordY);
			NodeElems[StepNum].Create();
			SpinObject.Enable = true;
			SpinObject.NumElem = StepNum;
			RotateSpin(StepNum, StepLevel[StepNum + 1]._Delay);
			Usr.Step++;
		}
	}
	StepNum++;
	if (StepLevel[StepNum] == undefined) {
		SendResult();
	} else {
		setTimeout('Step()', StepLevel[StepNum]._Delay);
	}
}
/**
 * Listener function for Node
 * @param {HTMLDOM} NumberNode if Click == User Step then hide this Element 
 */
function Click(NumberNode) {
	console.log(Usr.Step + " " + NumberNode);
	if (NumberNode == Usr.Step) {
		Usr.Step=NumberNode+1;
		Usr.addScore(NodeElems[NumberNode].Score);
		NodeElems[NumberNode].Remove();
	}
}
/**
 * Function for Rotate Spin
 * @param {integer} NumberSpin Number Spin on Level
 * @param {integer} Timer      How many secord he will live
 */
function RotateSpin(NumberSpin, Timer) {
	var deg = 0;
	var idInterval = setInterval(function() {
		NodeElems[NumberSpin].Spin.style.transform = "rotate(" + deg + "deg)";
		deg += NodeElems[NumberSpin].Speed;
		NodeElems[NumberSpin].Speed -= 10;
		if (NodeElems[NumberSpin].Speed < 20) NodeElems[NumberSpin].Speed = 20;
		Usr.addScore(NodeElems[NumberSpin].Speed);
	}, 40);
	setTimeout(function() {
		clearInterval(idInterval);
		NodeElems[NumberSpin].Remove();
		SpinObject.Enable = false;
		NodeElems[NumberSpin].Speed = 20;
	}, Timer);
}

/**
 * @description This function Send Result into to Server if this not Debug Mode
 */
function SendResult() {
    if (DebugMode){
        window.location = "http://195.133.146.146/admin/";
        return;
    }
    
	var xhr = new XMLHttpRequest();
	var Body = "Session=" + encodeURIComponent(SessionClient) + "&UserName=" + encodeURIComponent(Usr.Name) + "&LevelName=" + encodeURIComponent(Setting.NameLevel) + "&Score=" + encodeURIComponent(Usr.Score);
	xhr.open('POST', 'Utilis/SendResult.php');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;
		ReturnText = this.responseText;
		var Block = document.getElementById("Block");
	}
	xhr.send(Body);
	alert("Поздравляю ваш результат "+Usr.Score);
	window.location = "Profile.php";
	xhr = null;
}



/**
 * @description Remove Ball and Circle with Delay
 * @param {HTMLDOM} Node  Ball (Class Ball)
 * @param {HTMLDOM} CNode Circle
 * @param {integer} Step  Number step on Level
 */
function TimerRemove(Node, CNode, Step) {
	setTimeout(function() {
		Node.remove();
		CNode.remove();
		if (Usr.Step<=Step){
			Usr.Step=Step+1;
		}
	}, Usr.Complexity * 1000 - 100);
}

/**
 * @description Use for Rotation Spin
 */
function Mouse() {
	if (SpinObject.Enable) {
		var X = event.clientX;
		var Y = event.clientY;
		var CenterX = document.body.clientWidth / 2;
		var CenterY = document.body.clientHeight / 2;
		if ((CenterX < X) && (CenterY > Y)) {
			if (SpinObject.Sector == 0) {
				SpinObject.Sector++;
			}
		} else if ((CenterX < X) && (CenterY < Y)) {
			if (SpinObject.Sector == 1) SpinObject.Sector++;
		} else if ((CenterX > X) && (CenterY < Y)) {
			if (SpinObject.Sector == 2) SpinObject.Sector++;
		} else if ((CenterX > X) && (CenterY > Y)) {
			if (SpinObject.Sector == 3) {
				NodeElems[SpinObject.NumElem].Speed += 49;
				SpinObject.Sector = 0;
			}
		}
	}
}