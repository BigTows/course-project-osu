"use strict";
/**
* @author BigTows
* @version 1.5
*/
/**
* @member {HTML_DOM} Header Header on html
*/
var Header = document.getElementById("Header");
/**
* @member {HTML_DOM} Menu Div with ID Menu on html
*/
var Menu = document.getElementById("Menu");
/**
* @member {bool} MenuOpen 
*/
var MenuOpen = false;
if (window.innerWidth>=1280){
	MenuSlide();
}
/**
 * This function hide and show Menu Slider
 */
function MenuSlide() {
    var Width = document.documentElement.clientWidth;
    if (MenuOpen) {
        Menu.style.marginLeft = "-270px";
        Header.style.marginLeft = "0px";
        MenuOpen = false;
    } else {
        Menu.style.marginLeft = "0px";
        Header.style.marginLeft = "270px";
        MenuOpen = true;
    }
}