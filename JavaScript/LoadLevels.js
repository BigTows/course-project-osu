/**
* @member {HTML_DOM} xhr XMLHttpRequest var 
*/
var xhr = new XMLHttpRequest();
xhr.open('POST', 'Utilis/Levels.php');
xhr.onreadystatechange = function() {
  if (this.readyState != 4) return;
  ReturnText=this.responseText;
    /**
    * @member {HTML_DOM} Block Block on html
    */
  var Block = document.getElementById("Block");
  Block.innerHTML = ReturnText;
}
xhr.send();
xhr = null;