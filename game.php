<?php
/**
* Install Template game
*/
if (!isset($_SESSION)){
	session_start();
}
require_once "Utilis/Utilis.php";
if (isset($_GET['Level']) && isLoggin(session_id(),$_SERVER['REMOTE_ADDR'])){
	if (!LevelExist($_GET['Level'])){
		echo("Sorry, we can't find this map :(");
	}else{
		echo("<script>
			var ClientName ='".getName(session_id())."';
			var SessionClient='".session_id()."';
			var LevelName = '".$_GET['Level']."';
		</script>");
		ViewInc($_GET['Level']);
?>
		<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <link rel="stylesheet" href="Style/OsuStyle.css">
</head>
<body onmousemove="Mouse()" id="Zone">
		<div class="Panel" id="Panel">
			<div id="Complexity">
				Выберите сложность
			</div>
			<div class="Pick" id="Hard">
			</div>
			<div class="Pick" id="Medium">
			</div>
			<div class="Pick" id="Easy">
			</div>
		</div>
    <div id="Score">Score: 0</div>

</body>
<audio id="Music"></audio>
 	<script onstartload src="JavaScript/Levels/<?php echo $_GET['Level'] ?>.js"></script>
    <script onstartload src="JavaScript/Osu.js"></script>
<script>
	var Hard = document.getElementById("Hard");
	var Medium = document.getElementById("Medium");
	var Easy = document.getElementById("Easy");
	var Usr;
	var StepLevel;
    var DebugMode = false;
	var xhr = new XMLHttpRequest();
	xhr.open("get", "JavaScript/Levels/"+LevelName+".json",true);
	xhr.send();
	xhr.onreadystatechange = function(){
		if (this.readyState != 4) return;
		StepLevel = JSON.parse(this.responseText);
	};
	Audio.src="JavaScript/Levels/"+Setting.NameLevel+".mp3";
	Hard.addEventListener("click",function(){
		Start(1);
	});
	Medium.addEventListener("click",function(){
		Start(2.5);
	});
	Easy.addEventListener("click",function(){
		Start(4);
	});
	function Start(Num){
		Usr = new User(ClientName, Num);
		
		setTimeout('Step()', StepLevel[StepNum]._Delay);	
		document.getElementById("Panel").remove();
		Audio.load();
		Audio.play();
	}
</script>
</html>

<?php
	}
}else{
	header("Location: /",true,301);
}
?>