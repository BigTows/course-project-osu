<?php
/**
* In this script, the user closes the session goes
* @package / 
*/
if (!isset($_SESSION)){
	session_start();
}
require("Utilis/DBConnect.php");
/**
* Text var with SQL Query
* @var string SQL Query
*/
$QuerySQL = "DELETE FROM `Sessions` WHERE Session_ID = :Session";
try{
    /**
    * @var PDOStatement Using for work with DB
    */
	$Result = $DBConnect->prepare($QuerySQL);
    /**
    * @var string Session User
    */
	$S_ID = session_id();
	$Result->bindParam(':Session',$S_ID);
	$Result->execute();
	session_destroy();
	header("Location: /",true,301);
}catch(Exception $e){
	
}
?>