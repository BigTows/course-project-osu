<?php
/**
 * Install Template
 * @package /
*/
require_once "Utilis/config.php";
require_once "Utilis/Smarty.config.php";
require_once "Utilis/ReCaptcha.php";
if ($User->isLoggin()){
	header("Location: /",true,301);
	exit;
}
/**
* @var String block name which we will connect
*/
$Block="Registration";
$Smarty->assign("Block",$Block);
$Smarty->display("MainPage.tpl");

?>