<?php
/* Smarty version 3.1.30, created on 2016-11-25 22:49:14
  from "/var/www/html/template/MainPage.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_583895ba9fcdb7_36841899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '581b42107bbf0bdcea02321d0f21ceb73ab611d1' => 
    array (
      0 => '/var/www/html/template/MainPage.tpl',
      1 => 1480103350,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:User.UserBlock.tpl' => 1,
    'file:Guest.UserBlock.tpl' => 1,
    'file:User.AuthBlock.tpl' => 1,
    'file:Guest.AuthBlock.tpl' => 1,
  ),
),false)) {
function content_583895ba9fcdb7_36841899 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Game - Osu...</title>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Styles']->value, 'Style');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Style']->value) {
?>
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['Style']->value;?>
">
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		<?php echo '<script'; ?>
 src="JavaScript/NotifyS.js"><?php echo '</script'; ?>
>
		<?php if ($_smarty_tpl->tpl_vars['Block']->value == "Registration") {?>
		<?php echo '<script'; ?>
 src='https://www.google.com/recaptcha/api.js'><?php echo '</script'; ?>
>
		<?php }?>
	</head>
<body id="Body">
        <header id="Header">
            <ul>
                <li> <i class="icon-menu" id="icon-menu" onclick="MenuSlide()"></i> </li>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['TitlesMenu']->value, 'Title');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Title']->key => $_smarty_tpl->tpl_vars['Title']->value) {
$__foreach_Title_1_saved = $_smarty_tpl->tpl_vars['Title'];
?>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['Title']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['Title']->key;?>
</a></li>
                <?php
$_smarty_tpl->tpl_vars['Title'] = $__foreach_Title_1_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </ul>
        </header>
        <div id="Menu">
            <div class="user-block">
	           <?php if ($_smarty_tpl->tpl_vars['Loggin']->value == true) {?>
	           <?php $_smarty_tpl->_subTemplateRender("file:User.UserBlock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	           <?php } else { ?>
	           <?php $_smarty_tpl->_subTemplateRender("file:Guest.UserBlock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	           <?php }?>
            </div>
            <div class="auth-block">
	           <?php if ($_smarty_tpl->tpl_vars['Loggin']->value == true) {?>
	           <?php $_smarty_tpl->_subTemplateRender("file:User.AuthBlock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	           <?php } else { ?>
	           <?php $_smarty_tpl->_subTemplateRender("file:Guest.AuthBlock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	           <?php }?> 
            </div>
        </div>
        <div class="Block" id="Block">
	        <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['Block']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        </div>
        <?php echo '<script'; ?>
 src="JavaScript/Menu.js"><?php echo '</script'; ?>
>
        <!--<?php echo '<script'; ?>
 src="JavaScript/background.js"><?php echo '</script'; ?>
>!-->
    </body>

    </html>
<?php }
}
