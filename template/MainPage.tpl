
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Game - Osu...</title>
		{foreach $Styles as $Style}
		<link rel="stylesheet" href="{$Style}">
		{/foreach}
		<script src="JavaScript/NotifyS.js"></script>
		{if $Block == "Registration"}
		<script src='https://www.google.com/recaptcha/api.js'></script>
		{/if}
	</head>
<body id="Body">
        <header id="Header">
            <ul>
                <li> <i class="icon-menu" id="icon-menu" onclick="MenuSlide()"></i> </li>
                {foreach $TitlesMenu as $Title}
                <li><a href="{$Title}">{$Title@key}</a></li>
                {/foreach}
            </ul>
        </header>
        <div id="Menu">
            <div class="user-block">
	           {if $Loggin == true}
	           {include file='User.UserBlock.tpl'}
	           {else}
	           {include file='Guest.UserBlock.tpl'}
	           {/if}
            </div>
            <div class="auth-block">
	           {if $Loggin == true}
	           {include file='User.AuthBlock.tpl'}
	           {else}
	           {include file='Guest.AuthBlock.tpl'}
	           {/if} 
            </div>
        </div>
        <div class="Block" id="Block">
	        {include file="$Block.tpl"}
        </div>
        <script src="JavaScript/Menu.js"></script>
        <!--<script src="JavaScript/background.js"></script>!-->
    </body>

    </html>
