<?php
/**
 * Install Template
* @author BigTows
* @package Utilis
*/
require_once"Utilis/config.php";
require_once "Utilis/Utilis.php";
require_once("Utilis/Smarty.config.php");
if (!($User->isLoggin())){
	header("Location: /Registration.php",true,301);
	exit;
}
/**
* @var String block name which we will connect
*/
$Block="Profile";
$Records = getRecords(getName(session_id()));
$Smarty->assign("Block",$Block);
$Smarty->assign("Records",$Records);
$Smarty->display("MainPage.tpl");
?>
