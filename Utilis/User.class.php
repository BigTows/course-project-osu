<?php
/**
* User Class
* @package Utilis
*/
namespace Struct;
require_once 'config.php';
/**
 * User Class Stores Session, IP, Status Loggin and name User
 */
class User{
    /**
    * Stores User Name
    * @var string $UserName Name user
    */
	private $UserName;
    /**
    * Stores Status Loggin user
    *  @var Bool $Loggin Status user
    */
	private $Loggin;
    /**
    * Stores Session user
    * @var String $Session Session user
    */
	private $Session;
    /**
    * Stores User ip
    * @var String $IP IP user
    */
	private $IP;
	/**
	 * Create User
	 * @private
	 * @param String $Session Session User
	 * @param String $IP      IP User
	 */
	public function __construct($Session,$IP){
		$this->Session=$Session;
		$this->IP=$IP;

		require("DBConnect.php");
		$QuerySQL = 'SELECT * FROM `Sessions` WHERE `Session_ID` = :Session AND `ip` = :IP';
		try{
			$Result = $DBConnect->prepare($QuerySQL);
			$Result->bindparam(":Session",$Session);
			$Result->bindparam(":IP",$IP);
			$Result->execute();
			if ($Result->rowCount()>=1){
				$this->Loggin=true;
				$row=$Result->fetch();
				$this->UserName=$row['Name'];
			}else{
				$this->Loggin= false;
			}
		}catch(Exception $e){
			$this->Loggin= false;
		}
	}
	/**
	 * This function return name user
	 * @return String Name
	 */
	public function getName(){
		if ($this->Loggin){
			return $this->UserName;
		}
	}
	/**
	 * return Status "Loggin" user
	 * @return boolean Loggin status
	 */
	public function isLoggin(){
		return $this->Loggin;
	}


}

?>
