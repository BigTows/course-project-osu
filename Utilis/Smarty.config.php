<?php
/**
 * Smatry Config File
 * @package Utilis
 * @version 1.0
*/
include("config.php");
if (!isset($_SESSION)){
	session_start();
}
$NotFound=false;
require_once($_SERVER['DOCUMENT_ROOT']."/libs/Smarty.class.php");
require_once("Utilis.php");
require_once "User.class.php";
/**
 * @global Smarty Smarty Object
*/
$Smarty = new Smarty;
$Smarty->template_dir = "template";
$Smarty->compile_dir = "template/ready";
$Smarty->config_dir = "template/config";
$Smarty->cache_dir = "template/cache";

/**
 * @global Array With Link Styles
*/
$Styles = array(
	"Style/main.css",
	"Style/fontello.css",
	"Style/background.css",
	"Style/Notify.css",
	"https://fonts.googleapis.com/css?family=Roboto:100,300"
);
/**
* 
*/
$TitlesMenu = array(
	"Главная" => "/",
	"Рекорды" => "/Records.php",
	"Правила" => "/Rules.php"
);
$TitlesUserMenu = array(
	"Играть" => "/Levels.php",
	"Профиль" => "/Profile.php",
	"Мэйкер" => "/Maker.php",
	"Выход" => "/Exit.php"	
);
/**
 * @global User User var
*/
$User = new Struct\User(session_id(),$_SERVER['REMOTE_ADDR']);


$Smarty->assign("Styles",$Styles);
$Smarty->assign("TitlesMenu",$TitlesMenu);
$Smarty->assign("TitlesUserMenu",$TitlesUserMenu);
$Smarty->assign("Loggin",$User->isLoggin());
$Smarty->assign("NotFound",$NotFound);
?>
