<?php
/**
 * Thus config file
  * @author BigTows
  * @version 1.0.2
  */

/**
* @var boolean $Debug if true then activate mode debug (Show more information) 
*/
$Debug = true;

if ($Debug) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
?>
