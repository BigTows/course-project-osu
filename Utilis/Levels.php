<?php
/**
* In this file returns a table with the Levels
* @package Utilis
*/
	require_once("DBConnect.php");
	require("config.php");
/**
* @var String ver with SQL Query
*/
	$QuerySQL = "SELECT * FROM `Levels` ORDER BY Views DESC";
	try{
        /**
        * @var PDOStatement $Result Result after Prepare Query
        * @var Array $row Array
        */
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->execute();
		while ($row=$Result->fetch()){
		echo '	
			<div class="boxLevel">
			<a href="/game.php?Level='.$row["Name"].'" id="NameLevel">
			'.$row["Name"].'
			</a>
			<i id="SongLevel">
			 Песня: '.$row["SongName"].'
			</i>
			<a id="StatLike">
				Автор: '.$row["Author"].'
			</a>
			<a id="StatView">
				Просмотров: '.$row["Views"].'
			</a>
		</div>';
		}
	}catch(PDOException $e){
		
	}	
?>