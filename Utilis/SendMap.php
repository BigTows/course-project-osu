<?php
/**
 * Upload files for moderation
 * @package Usilis 
*/
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
require_once 'DBConnect.php';
if (isset($_POST['LevelJSON'])){
	$Path=$_SERVER['DOCUMENT_ROOT'] ."/Maps/".$_POST['UserName']."/".$_POST['LevelName']."/";
	mkdir($Path,0777,true);
	$File = fopen($Path.$_POST['LevelName'].".json", "w");
	fwrite($File, $_POST['LevelJSON']);
	fclose($File);
	$File = fopen($Path.$_POST['LevelName'].".js", "w");
	fwrite($File,"var Setting={NameLevel: '".$_POST['LevelName']."',Window:{width:".$_POST['WindowW'].",height:".$_POST['WindowH']."}}");
	fclose($File);
	if (move_uploaded_file($_FILES["Music"]["tmp_name"],$Path.basename($_FILES["Music"]["name"]))){
		rename($Path.$_FILES["Music"]["name"],$Path.$_POST['LevelName'].".mp3");
		echo("We Upload This Map");
		$QuerySQL = "INSERT INTO `Demo_Levels`(`User`, `LevelName`) VALUES (:Name,:LevelName)";
		try{
			$Result = $DBConnect->prepare($QuerySQL);
			$Result->bindParam(":Name",$_POST['UserName']);
			$Result->bindParam(":LevelName",$_POST['LevelName']);
			$Result->execute();
		}catch(Exception $e){
			return false;
		}

	}

}
?>
