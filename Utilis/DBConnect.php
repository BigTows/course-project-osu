<?php
/**
* File with Settings DataBase. Use for Connect to MySQL Server
* @package Utilis
*/
require_once "config.php";
/**
 * @var string $DBname Name DataBase.
 * @var string $DBHost Host DataBase.
 * @var string $DBUser User DataBase.
 * @var string $DBPassword Password User from DataBase.
 * @var string $Request host to MySQL Server
 * @var PDO $DBConnect PDO Object
 */
$DBName     = "Osu";
$DBHost     = "localhost";
$DBUser     = "root";
$DBPassword = "c4e07XMEoFsyyHWAhFAbJalvd";
try {
    $Request = "mysql:host=$DBHost;dbname=$DBName";
    $DBConnect = new PDO($Request,$DBUser,$DBPassword);
}
catch (Exception $e) {
   if ($Debug) {
       echo $e;
        echo '
          <script>
           var NotifyProp = {
                Position: "center",
                Text: "Ошибка подключения.",
                Focus: true,
                Style: "Info",
                Close: 5
            }
            Notify(NotifyProp);
            </script>
           ';
    }
}
?>
