<?php
/**
* This Utilis File with function
* @package Utilis
*/
/**
 * Return false if no records where a match session and IP 
 * @param  string  $Session Session User
 * @param  String  $IP      IP User
 * @return boolean Return false if no records where a match session and IP 
 */
function isLoggin($Session,$IP){
	require("DBConnect.php");
	$QuerySQL = 'SELECT * FROM `Sessions` WHERE `Session_ID` = :Session AND `ip` = :IP';
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindparam(":Session",$Session);
		$Result->bindparam(":IP",$IP);
		$Result->execute();
		if ($Result->rowCount()>=1){
			return true;
		}else{
			return false;
		}
	}catch(Exception $e){
		return false;
	}
}
/**
 * Return true if the session belongs to Admin
 * @param  string  $Session Session User
 * @return boolean Return true if the session belongs to Admin
 */
function isAdmin($Session){
    require("DBConnect.php");
	$QuerySQL = 'SELECT * FROM `Sessions` WHERE `Session_ID` = :Session';
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindparam(":Session",$Session);
		$Result->execute();
		//Add if Not valid Session
		$row = $Result->fetch();
        $QuerySQL = 'SELECT * FROM `Admins` WHERE `Name`= :Name';
         try{
            $Result = $DBConnect->prepare($QuerySQL);
		$Result->bindparam(":Name",$row['Name']);
		$Result->execute(); 
        if ($Result->rowCount()===1){
            return true;
        }else{
            return false;
        }
         }catch(Exception $e){
             return false;
         }
	}catch(Exception $e){
		return false;
	}
}
/**
 * Return Name from Session
 * @param string         $Session Session User 
 * @return string return Name User if DBConnect failed then return false(bool)
 */
function getName($Session){
	require("DBConnect.php");
	$QuerySQL = 'SELECT * FROM `Sessions` WHERE `Session_ID` = :Session';
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindparam(":Session",$Session);
		$Result->execute();
		/**
        * @todo add Check valid Session
        */
		$row = $Result->fetch();
		return  $row['Name'];
	}catch(Exception $e){
		return false;
	}
}
/**
 * Return true if Registation Access else false
 * @param  String  $Name  Name new User
 * @param  String  $Email Email new User
 * @return boolean if all okay return true
 */
function RegAccess($Name,$Email){
	require("DBConnect.php");
	$QuerySQL = "SELECT * FROM `Users` WHERE `Name` = :Name OR `Email` = :Email";
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(":Name",$Name);
		$Result->bindParam(":Email",$Email);
		$Result->execute();
		if ($Result->rowCount()===0){
			return true;
		}else{
			return false;
		}

	}catch(Exception $e){
		return false;
	}

}
/**
 * Add User to DataBase
 * @param String $Name  Name User
 * @param String $Pass  Password with hash SHA1
 * @param String $Email Email User
 */
function Registration($Name,$Pass,$Email){
	require("DBConnect.php");
	$QuerySQL = "INSERT INTO `Users`(`Name`, `Password`, `Email`) VALUES (:Name,:Password,:Email)";
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(":Name",$Name);
		$Result->bindParam(":Password",$Pass);
		$Result->bindParam(":Email",$Email);
		$Result->execute();
	}catch(Exception $e){

	}

}
/**
 * Add Session and IP to DataBase
 * @param  String  $Name    User name
 * @param  String  $Session Session user
 * @param  String  $IP      IP user
 * @return boolean if is Loggin then return false or failed DBConnect and true if All okay
 */
function Loggin($Name,$Session,$IP){
	require("DBConnect.php");
	if (isLoggin($Session,$IP)){
		return false;
	}
	$QuerySQL = "INSERT INTO `Sessions`(`Name`, `Session_ID`, `ip`) VALUES (:Name,:Session,:IP)";
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(":Name",$Name);
		$Result->bindParam(":Session",$Session);
		$Result->bindParam(":IP",$IP);
		$Result->execute();
        return true;
	}catch(Exception $e){
        return false;
	}
}
if (isset($_POST['LogginAuth'])){
	require_once 'DBConnect.php';
	if (!isset($_SESSION)){
		session_start();
	}
	$UserName = $_POST['UserName'];
	$Password = sha1($_POST['Password']);
	$Session = session_id();
	$QuerySQL = 'SELECT * FROM `Users` WHERE `Name` = :UserName AND `Password` = :Password';
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(':UserName',$UserName);
		$Result->bindParam(':Password',$Password);
		$Result->execute();
		if ($Result->rowCount() === 1){
			Loggin($_POST['UserName'],$Session,$_SERVER['REMOTE_ADDR']);
		}else{

			$NotFound = true;
		}
	}catch(Exception $e){
		echo 'Erre';
	}
}
/**
 * Check Level
 * @param  String  $LevelName Name Level
 * @return boolean return true if this map Exist else false
 */
function LevelExist($LevelName){
	require "DBConnect.php";
	$QuerySQL = 'SELECT * FROM `Levels` WHERE `Name` = :Name';
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(':Name',$LevelName);
		$Result->execute();
		if ($Result->rowCount() === 1){
			return true;
		}else{
			return false;
		}
	}catch(Exception $e){
		echo 'Erre';
	}
}
/**
 * Inc view this map
 * @param String $Map Name Level
 */
function ViewInc($Map){
	require "DBConnect.php";
	$QuerySQL = "UPDATE `Levels` SET `Views`=`Views`+1 WHERE `Name`=:Name";
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(':Name',$Map);
		$Result->execute();
	}catch(Exception $e){
		echo 'Erre';
	}

}
/**
 * return Personal Records
 * @param  String $Name User name
 * @return String Return Message with error or HTML code with Records on table
 */
function getRecords($Name){
	require "DBConnect.php";
	$Message = "<center><table
	style='background-color:#818181;'
	 border='1'>
   <caption>Рекорды карт</caption>
   <tr>
    <th>Имя карты</th>
    <th>Рекорд</th>
   </tr>";
	$CountRecords = 0;
	$QuerySQL = "SELECT * FROM `Records` WHERE Name = :Name";
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(':Name',$Name);
		$Result->execute();
		while($row = $Result->fetch()){
			$Message = $Message . "<tr><td>".$row['Level_Name']."</td><td>".$row['Points']."</td>";
			$CountRecords++;
		}
	}catch(Exception $e){
		$Message="Error";
	}
	$Message=$Message."</table></center>";
	if ($CountRecords==0){
		$Message="У вас нет рекордов.";
	}
	return $Message;
}
/**
 * Set record into DataBase where match User and Level, if user don't exist then Create user and add.
 * @param String  $Name      User name
 * @param string  $LevelName Name level
 * @param integer $Score     How many set Score
 */
function SendRecord($Name,$LevelName,$Score){
	require "DBConnect.php";
	$QuerySQL = "SELECT * FROM `Records` WHERE Name = :Name AND Level_Name=:LevelName";
	try{
		$Result = $DBConnect->prepare($QuerySQL);
		$Result->bindParam(':Name',$Name);
		$Result->bindParam(':LevelName',$LevelName);
		$Result->execute();
		if ($Result->rowCount()==0){
			$QuerySQL = "INSERT INTO `Records` VALUES(:Name,:LevelName,:Score)";
			try{
				$Result = $DBConnect->prepare($QuerySQL);
				$Result->bindParam(':Name',$Name);
				$Result->bindParam(':LevelName',$LevelName);
				$Result->bindParam(':Score',$Score);
				$Result->execute();
			}catch(Exception $e){
				echo 'Erre';
			}
		}else{
			$QuerySQL = "UPDATE `Records` Set Points=:Score WHERE Name = :Name AND Level_Name=:LevelName AND Points<:Score";
			try{
				$Result = $DBConnect->prepare($QuerySQL);
				$Result->bindParam(':Name',$Name);
				$Result->bindParam(':LevelName',$LevelName);
				$Result->bindParam(':Score',$Score);
				$Result->execute();
			}catch(Exception $e){
				echo 'Erre';
			}
		}
	}catch(Exception $e){
		echo 'Erre';
	}
}

?>