<?php
/**
 * Registration file with ReCaptcha
 * @package Utilis
* @todo Clean code
*/
if (isset($_POST['SendForm'])){
	require_once("Utilis.php");
	$BadInput = false;
	$Login = $_POST['RegLogin'];
	$F_Pswd = $_POST['FirstPassword'];
	$S_Pswd = $_POST['SecondPassword'];
	$Email  = $_POST['Email'];
	$Message = "";
	if (preg_match("^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$^", $Login)==0){
		$Message = "Логин имеет недопустимые символы. ";
		$BadInput = true;
	}
	if (($S_Pswd!=$F_Pswd) or (strlen($F_Pswd)<6)){
		$Message=$Message."Пароли должны совпадать и быть более 6 символов. ";
		$BadInput = true;
	}
	/*
     * ReCaptcha
    */
	$UrlGoogle = "https://www.google.com/recaptcha/api/siteverify";
	$PrivateKey = "6Le--QwUAAAAALwyW16aAkSmT6X_hYEloEKotygp";
	$Response = file_get_contents($UrlGoogle."?secret=".$PrivateKey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	$json = json_decode($Response);
	if (isset($json->success) && ($json->success==true) && !$BadInput){
		
		if (RegAccess($Login,$Email)){
			Registration($Login,sha1($F_Pswd),$Email);
			Loggin($Login,session_id(),$_SERVER['REMOTE_ADDR']);
			header("Location: /",true,301);
			exit;
		}else{
			$Message=$Message."Логин или Email заняты. ";
		}
	}else{
		echo "<script> var Mes='".$Message."';</script>";
		?>
		<script>
			window.onload = function(){
			var Board = document.getElementById("InfoPanel");
			Board.innerHTML=Mes;
			}
		</script>
		<?php
	}

}
?>