<?php
/**
 * This is a template file for a function Maker
 * @package /
*/
if (!isset($_SESSION)){
	session_start();
}
require_once "Utilis/Utilis.php";
if (isLoggin(session_id(),$_SERVER['REMOTE_ADDR'])){
?>
    <html>

    <head>
        <link rel="stylesheet" href="Style/OsuStyle.css">
        <meta charset="utf-8">
    </head>

    <body>
        <style>
            .Settings {
                float: left;
                border-bottom: 1px solid #000;
                width: 100%;
            }
            
            #LevelCode {
                resize: none;
                width: 600px;
                height: 50px;
            }
            
            #Music {}

        </style>
        <script>
            var ClientName = '<?php echo getName(session_id()); ?>';
            var SessionClient = '<?php echo session_id(); ?>';

        </script>
        <div class="Settings">
            <input type="text" name="NameLevel" id="NameLevel" placeholder="Name Level">
            <input name="Music" id="Music" type="file">
        </div>
        <audio controls id="Player">
    </audio>
        <div id="Zone">
            <script src="JavaScript/Maker.js"></script>
        </div>
    </body>

    </html>
    <?php	
}else{
	header("Location: /",true,301);
}
?>
