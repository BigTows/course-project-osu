*version 2.1#
##Last Updates##
*GLOBAL UPDATE v2*
#
- ReWrite Osu.js
- ReWrite User.class.php
- Add Smarty
- Add Reg Mode
- add ReCaptcha
- add Profile
- Rewrite Maker.js
- and Fix Bugs
#